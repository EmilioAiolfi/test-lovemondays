'use strict'

describe 'lodash', ->
  beforeEach module('lovemondaysApp')
  describe '_', ->
    it 'should be defined', inject((_) ->
      expect(_).not.toBeNull
    )

    it 'should have toArray() defined', inject((_) ->
      expect(_.toArray).toBeDefined()
    )
