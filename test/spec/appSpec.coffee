'use strict'

describe 'Testing Modules', ->
  describe 'App Module:', ->

    module = undefined

    beforeEach ->
      module = angular.module('lovemondaysApp')

    it 'should be registered', ->
      expect(module).not.toBe null

    describe 'Dependencies:', ->
      deps = undefined
      hasModule = (m) ->
        deps.indexOf(m) >= 0

      beforeEach ->
        deps = module.value('appName').requires

      #you can also test the module's dependencies

      it 'should have ngAnimate as a dependency', ->
        expect(hasModule('ngAnimate')).toBe true

      it 'should have ngAria as a dependency', ->
        expect(hasModule('ngAria')).toBe true

      it 'should have ngCookies as a dependency', ->
        expect(hasModule('ngCookies')).toBe true

      it 'should have ngResource as a dependency', ->
        expect(hasModule('ngResource')).toBe true

      it 'should have ngSanitize as a dependency', ->
        expect(hasModule('ngSanitize')).toBe true

      it 'should have ngTouch as a dependency', ->
        expect(hasModule('ngTouch')).toBe true

      it 'should have ui.bootstrap as a dependency', ->
        expect(hasModule('ui.bootstrap')).toBe true

      it 'should have ui.router as a dependency', ->
        expect(hasModule('ui.router')).toBe true

      it 'should have angular-loading-bar as a dependency', ->
        expect(hasModule('angular-loading-bar')).toBe true

      it "should have lovemondaysApp.services as a dependency", ->
        expect(hasModule('lovemondaysApp.services')).toBe true

      it 'should have lovemondaysApp.controllers as a dependency', ->
        expect(hasModule('lovemondaysApp.controllers')).toBe true



describe 'App Config Phase:', ->

  describe 'html5Mode', ->
    $locationProvider = undefined

    beforeEach ->
      angular.module('locationProviderConfig', []).config (_$locationProvider_) ->
        $locationProvider = _$locationProvider_
        spyOn $locationProvider, 'html5Mode'

      module 'locationProviderConfig'
      module 'lovemondaysApp'
      inject()

    it 'should set html5 mode', ->
      expect($locationProvider.html5Mode).toHaveBeenCalledWith true



  describe 'Testing States', ->

    describe 'states', ->
      rootScope = undefined
      state = undefined
      location = undefined
      beforeEach angular.mock.module('ui.router')
      beforeEach angular.mock.module('lovemondaysApp')

      beforeEach inject(($rootScope, $state, $location, $templateCache) ->
        rootScope = $rootScope
        state = $state
        location = $location
        $templateCache.put('views/404.html', '404 - Not Found')
      )

      it 'should respond home state', ->
        expect(state.href('profile')).toEqual '/'

      it 'init path is correct', ->
        rootScope.$emit '$locationChangeSuccess'
        expect(location.path()).toBe '/'

      it 'redirects to otherwise page after locationChangeSuccess', ->
        location.path '/nonExistentPath'
        rootScope.$digest()
        expect(location.path()).toBe '/404'
