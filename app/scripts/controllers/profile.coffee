'use strict'

###*
 # @ngdoc function
 # @name lovemondaysApp.controller:ProfileCtrl
 # @description
 # # ProfileCtrl
 # Controller of the lovemondaysApp
###

lovemondaysAppControllers = angular.module 'lovemondaysApp.controllers'

lovemondaysAppControllers
  .controller 'ProfileCtrl', ($scope, _, $uibModal) ->

    $scope.open = () ->
      modalInstance = $uibModal.open(
        animation: true
        templateUrl: 'views/modal/login.html'
        controller: 'ModalInstanceCtrl'
        backdrop: 'static',
        size: 'sm'
      )


lovemondaysAppControllers
  .controller 'ModalInstanceCtrl', ($scope, $uibModalInstance) ->

    $scope.cancel = ->
      $uibModalInstance.dismiss 'cancel'
      return
