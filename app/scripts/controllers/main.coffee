'use strict'

###*
 # @ngdoc function
 # @name lovemondaysApp.controller:MainCtrl
 # @description
 # # MainCtrl
 # Controller of the lovemondaysApp
###

lovemondaysAppControllers = angular.module 'lovemondaysApp.controllers'

lovemondaysAppControllers
  .controller 'MainCtrl', ($scope, $http, $timeout, $log) ->

    $scope.toggle = false;
    $scope.selected = undefined;

    $scope.getLocation = (val) ->
      $http.get('http://maps.googleapis.com/maps/api/geocode/json',
        params:
          address: val
          sensor: false
      ).then (res) ->
        addresses = []
        angular.forEach res.data.results, (item) ->
          addresses.push item.formatted_address

        #you can do client-side formatting here
        console.log('addresses', addresses)
        return addresses


    # $scope.getCompany = (val) ->
    #   $http.get('https://lovemondays.com.br/pesquisa/empresa/autocomplete',
    #     params:
    #       q: val
    #
    #   ).then (response) ->
    #     response.data.results.map (item) ->
    #       console.log 'company', item
    #       item.results
