'use strict'

###*
 # @ngdoc overview
 # @name lovemondaysApp
 # @description
 # # lovemondaysApp
 #
 # Main module of the application.
###

# Submodules
# angular.module 'lovemondaysApp.directives', []
angular.module 'lovemondaysApp.services', []
angular.module 'lovemondaysApp.controllers', []


angular
  .module('lovemondaysApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'ui.router',
    'angular-loading-bar',

    'lovemondaysApp.services',
    'lovemondaysApp.controllers'
  ])
  .config (
    $stateProvider,
    $httpProvider,
    $urlRouterProvider,
    $locationProvider,
    $sceDelegateProvider,
    $interpolateProvider,
    cfpLoadingBarProvider
  ) ->
    $locationProvider.html5Mode true
    cfpLoadingBarProvider.latencyThreshold = 100;
    $sceDelegateProvider.resourceUrlWhitelist [
      # Allow same origin resource loads.
      "self"
      # Allow loading from our assets domain.  Notice the difference between * and **.
      "data:**"
    ]

    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

    $interpolateProvider.startSymbol "{{"
    $interpolateProvider.endSymbol "}}"

    $stateProvider
      .state('profile',
        url: '/'
        templateUrl: 'views/profile.html'
        controller: 'ProfileCtrl'
      )
      .state('404',
        url: '/404'
        templateUrl: 'views/404.html'
      )

    $urlRouterProvider.otherwise ($injector) ->
      $injector.invoke ($state) ->
        $state.transitionTo '404', {}, false
        return
      return
