'use strict'

###*
 # @ngdoc service
 # @name lovemondaysApp.lodash
 # @description
 # # lodash
 # Service in the lovemondaysApp.
###
lovemondaysAppServices = angular.module 'lovemondaysApp.services'

lovemondaysAppServices
  .factory '_', ($window) ->
    # place lodash include before angular
    return $window._
