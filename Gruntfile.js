'use strict';

module.exports = function (grunt) {

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Automatically load required Grunt tasks
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin',
    ngtemplates: 'grunt-angular-templates',
    cdnify: 'grunt-google-cdn',
    grunticon: 'grunt-grunticon'
  });

  // Feature: generator prompt for html5mode support
  var modRewrite = require('connect-modrewrite')([
    '!\\.ttf|\\.woff|\\.ttf|\\.eot|\\.html|\\.js|\\.coffee|\\.css|\\.png|\\.jpg|\\.gif|\\.svg$ /index.html [L]'
  ]);

  // Configurable paths for the application
  var appConfig = {
    app: require('./bower.json').appPath || 'app',
    dist: 'dist'
  };

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    lovemondays: appConfig,

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep']
      },
      coffee: {
        files: ['<%= lovemondays.app %>/scripts/{,*/}*.{coffee,litcoffee,coffee.md}'],
        tasks: ['newer:coffee:dist']
      },
      coffeeTest: {
        files: ['test/spec/{,*/}*.{coffee,litcoffee,coffee.md}'],
        tasks: ['newer:coffee:test', 'karma']
      },
      sass: {
        files: ['<%= lovemondays.app %>/styles/{,*/}*.{scss,sass}'],
        tasks: ['sass:server', 'postcss:server']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      svgs: {
        files: '<%= lovemondays.app %>/images/svg/*.svg',
        tasks: ['svgmin', 'svgstore', 'svg2png'],
        options: {
          livereload: true,
        },
      },

      livereload: {
        files: [
          '<%= lovemondays.app %>/{,*/}*.html',
          '.tmp/styles/{,*/}*.css',
          '.tmp/scripts/{,*/}*.js',
          '<%= lovemondays.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
        ]
      }
    },

    // The actual grunt server settings
    browserSync: {
      livereload: {
        bsFiles: {
          src: [
            '<%= lovemondays.app %>/{,*/}*.html',
            '.tmp/styles/{,*/}*.css',
            '<%= lovemondays.app %>/scripts/{,*/}*.js',
            '<%= lovemondays.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
          ]
        },
        options: {
          watchTask: true,

          server: {
            proxy: {
              modRewrite,
              middleware: function (req, res, next) {
                res.setHeader('Access-Control-Allow-Origin', '*');
                next();
              }
            },
            baseDir: ['<%= lovemondays.app %>', '.tmp'],
            routes: {
              '/bower_components': 'bower_components'
            }
          }
        }
      },
      dist: {
        bsFiles: {
          src: '<%= lovemondays.dist %>/{,*/}*.html'
        },
        options: {
          server: {
            baseDir: '<%= lovemondays.dist %>'
          }
        }
      },
      test: {
        bsFiles: {
          src: 'test/spec/{,*/}*.js'
        },
        options: {
          watchTask: true,
          port: 9001,
          open: false,
          logLevel: 'silent',
          server: {
            baseDir: ['test', '.tmp', appConfig.app],
            routes: {
              '/bower_components': 'bower_components'
            }
          }
        }
      }
    },

    // Lint our coffee files
    // Linting is unobtrusive. If linting errors happen then they wont break the process

    coffeelint: {
      options: {
        force: true,  // Display lint errors as warnings. Do not break.
        configFile: 'coffeelint.json'
      },
      files: [
        'test/**/*.coffee',
        'src/**/*.coffee'
      ]
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: {
        src: [
          'Gruntfile.js'
        ]
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= lovemondays.dist %>/{,*/}*',
            '!<%= lovemondays.dist %>/.git{,*/}*'
          ]
        }]
      },
      server: '.tmp'
    },

    // Add vendor prefixed styles
    postcss: {
      options: {
        processors: [
          require('autoprefixer')({browsers: ['last 2 version']})
        ]
      },
      server: {
        options: {
          map: true
        },
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '{,*/}*.css',
          dest: '.tmp/styles/'
        }]
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '{,*/}*.css',
          dest: '.tmp/styles/'
        }]
      }
    },

    // Automatically inject Bower components into the app
    wiredep: {
      app: {
        src: ['<%= lovemondays.app %>/index.html'],
        ignorePath:  /\.\.\//,
        exclude: [ 'jquery' ],

      },
      test: {
        devDependencies: true,
        src: '<%= karma.unit.configFile %>',
        ignorePath:  /\.\.\//,
        fileTypes:{
          coffee: {
            block: /(([\s\t]*)#\s*?bower:\s*?(\S*))(\n|\r|.)*?(#\s*endbower)/gi,
              detect: {
                js: /'(.*\.js)'/gi,
                coffee: /'(.*\.coffee)'/gi
              },
            replace: {
              js: '\'{{filePath}}\'',
              coffee: '\'{{filePath}}\''
            }
          }
        }
      },
      sass: {
        src: ['<%= lovemondays.app %>/styles/{,*/}*.{scss,sass}'],
        ignorePath: /(\.\.\/){1,2}bower_components\//,
        exclude: [ 'bootstrap-sass-official/assets/stylesheets/_bootstrap.scss' ]
      }
    },

    // Compiles CoffeeScript to JavaScript
    coffee: {
      options: {
        sourceMap: true,
        sourceRoot: ''
      },
      dist: {
        files: [{
          expand: true,
          cwd: '<%= lovemondays.app %>/scripts',
          src: '{,*/}*.coffee',
          dest: '.tmp/scripts',
          ext: '.js'
        }]
      },
      test: {
        files: [{
          expand: true,
          cwd: 'test/spec',
          src: '{,*/}*.coffee',
          dest: '.tmp/spec',
          ext: '.js'
        }]
      }
    },

    // Compiles Sass to CSS and generates necessary files if requested
    sass: {
      options: {
          includePaths: [
              'bower_components'
          ]
      },
      dist: {
          files: [{
              expand: true,
              cwd: '<%= lovemondays.app %>/styles',
              src: ['*.scss'],
              dest: '.tmp/styles',
              ext: '.css'
          }]
      },
      server: {
          files: [{
              expand: true,
              cwd: '<%= lovemondays.app %>/styles',
              src: ['*.scss'],
              dest: '.tmp/styles',
              ext: '.css'
          }]
      }
    },

    // Renames files for browser caching purposes
    filerev: {
      dist: {
        src: [
          '<%= lovemondays.dist %>/scripts/{,*/}*.js',
          '<%= lovemondays.dist %>/styles/{,*/}*.css',
          '<%= lovemondays.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
          '<%= lovemondays.dist %>/styles/fonts/*'
        ]
      }
    },

    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
      html: '<%= lovemondays.app %>/index.html',
      options: {
        dest: '<%= lovemondays.dist %>',
        flow: {
          html: {
            steps: {
              js: ['concat', 'uglifyjs'],
              css: ['cssmin']
            },
            post: {}
          }
        }
      }
    },

    // Performs rewrites based on filerev and the useminPrepare configuration
    usemin: {
      html: ['<%= lovemondays.dist %>/{,*/}*.html'],
      css: ['<%= lovemondays.dist %>/styles/{,*/}*.css'],
      js: ['<%= lovemondays.dist %>/scripts/{,*/}*.js'],
      options: {
        assetsDirs: [
          '<%= lovemondays.dist %>',
          '<%= lovemondays.dist %>/images',
          '<%= lovemondays.dist %>/styles'
        ],
        patterns: {
          js: [[/(images\/[^''""]*\.(png|jpg|jpeg|gif|webp|svg))/g, 'Replacing references to images']]
        }
      }
    },

    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= lovemondays.app %>/images',
          src: '{,*/}*.{png,jpg,jpeg,gif}',
          dest: '<%= lovemondays.dist %>/images'
        }]
      }
    },

    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= lovemondays.app %>/images/svg',
          src: '{,*/}*.svg',
          dest: '<%= lovemondays.app %>/images/svg'
        }]
      }
    },

    svgstore: {
      options: {
        prefix : 'icon-', // This will prefix each ID
        svg: {
          viewBox : '0 0 50 50',
          xmlns: 'http://www.w3.org/2000/svg',
          style: 'display: none;'
        },
        cleanup: ['fill'],
        formatting : {
          indent_size : 2
        }
      },
      default : {
        files: {
          '<%= lovemondays.app %>/images/svg/build/svg-defs.svg': ['<%= lovemondays.app %>/images/svg/*.svg']
        }
      }
    },

    svginjector: {
      icons: {
        files: {
          '<%= lovemondays.app %>/scripts/vendor/svg-inject.js': ['<%= lovemondays.app %>/images/svg/build/svg-defs.svg']
        },
        options: {
          container: '#svg-container'
        }
      }
    },

    svg2png: {
      dist: {
        files: [{
          flatten: true,
          cwd: '<%= lovemondays.app %>/images/svg/',
          src: ['*.svg'],
          dest: '<%= lovemondays.app %>/images/svg/png' }
        ]
      }
    },

    htmlmin: {
      dist: {
        options: {
          collapseWhitespace: true,
          conservativeCollapse: true,
          collapseBooleanAttributes: true,
          removeCommentsFromCDATA: true
        },
        files: [{
          expand: true,
          cwd: '<%= lovemondays.dist %>',
          src: ['*.html'],
          dest: '<%= lovemondays.dist %>'
        }]
      }
    },

    ngtemplates: {
      dist: {
        options: {
          module: 'lovemondaysApp',
          htmlmin: '<%= htmlmin.dist.options %>',
          usemin: 'scripts/scripts.js'
        },
        cwd: '<%= lovemondays.app %>',
        src: 'views/{,*/}*.html',
        dest: '.tmp/templateCache.js'
      }
    },


    // ng-annotate tries to make the code safe for minification automatically
    // by using the Angular long form for dependency injection.
    ngAnnotate: {
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/concat/scripts',
          src: '*.js',
          dest: '.tmp/concat/scripts'
        }]
      }
    },

    // Replace Google CDN references
    cdnify: {
      dist: {
        html: ['<%= lovemondays.dist %>/*.html']
      }
    },

    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= lovemondays.app %>',
          dest: '<%= lovemondays.dist %>',
          src: [
            '*.{ico,png,txt}',
            '*.html',
            'images/{,*/}*.{webp}',
            'styles/fonts/{,*/}*.*'
          ]
        }, {
          expand: true,
          cwd: '.tmp/images',
          dest: '<%= lovemondays.dist %>/images',
          src: ['generated/*']
        }, {
          expand: true,
          cwd: '.',
          src: 'bower_components/bootstrap-sass-official/assets/fonts/bootstrap/*',
          dest: '<%= lovemondays.dist %>'
        }]
      },
      scripts: {
        expand: true,
        cwd: '<%= lovemondays.app %>/scripts/vendor',
        dest: '.tmp/scripts/vendor',
        src: '{,*/}*.js'
      },
      images: {
        expand: true,
        cwd: '<%= lovemondays.app %>/images',
        dest: '.tmp/images/',
        src: '{,*/}*.*'
      },
      styles: {
        expand: true,
        cwd: '<%= lovemondays.app %>/styles',
        dest: '.tmp/styles/',
        src: '{,*/}*.css'
      },
      fonts: {
        expand: true,
        flatten: true,
        cwd: '.',
        src: ['bower_components/bootstrap-sass-official/assets/fonts/bootstrap/*', 'bower_components/font-awesome/fonts/*'],
        dest: '.tmp/fonts/',
        filter: 'isFile'
      }
    },

    // Run some tasks in parallel to speed up the build process
    concurrent: {
      server: [
        'sass:server',
        'coffee:dist',
        'coffeelint',
        'copy:scripts',
        'copy:styles',
        'svgmin',
        'svgstore',
        'svginjector',
        'svg2png',
        'copy:fonts'
      ],
      test: [
        'coffee',
        'copy:styles',
        'copy:fonts'
      ],
      dist: [
        'coffee',
        'sass:dist',
        'copy:styles',
        'copy:fonts',
        'imagemin',
        'svgmin'
      ]
    },

    // Test settings
    karma: {
      unit: {
        configFile: 'test/karma.conf.coffee',
        singleRun: true
      }
    }
  });


  grunt.registerTask('serve', 'Compile then start a BrowserSync web server', function (target) {
    if (target === 'dist') {
      return grunt.task.run(['build', 'browserSync:dist']);
    }

    grunt.task.run([
      'clean:server',
      'wiredep',
      'concurrent:server',
      'postcss:server',
      'browserSync:livereload',
      'watch'
    ]);
  });


  grunt.registerTask('test', [
    'clean:server',
    'wiredep',
    'concurrent:test',
    'postcss',
    'browserSync:test',
    'karma'
  ]);

  grunt.registerTask('default', [
    'newer:jshint',
    'test',
    'build'
  ]);
};
